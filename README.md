# Headwind kiosk client sdk

## Подключение

Для подключения sdk к проекту загрузите файл [библиотеки](https://gitlab.com/headwind/kiosk-client-sdk/blob/master/aars/sdk12.aar),
затем подкючите ее через меню File -> New Module -> Import JAR/AAR.
Подключите созданный модуль к Вашему проекту.

## Вызов функций киоска

В AndroidManifest.xml добавьте разрешение

    <uses-permission android:name="ru.headwind.kiosk.actions"/>

### Поиск обновлений
В любом подходящем месте в коде вызовите 

    KioskSdk.launchScanStorages(@NonNull Context context);
    
Либо вручную вызвать интент

    Intent intent = new Intent("ru.headwind.kiosk.SCAN_STORAGE");
    context.sendBroadcast(intent);

## Получение сообщений из приложения киоска
Создайте наследника класса ru.headwind.kiosk.sdk.KioskReceiver,
затем объявите его в AndroidManifest.xml подобным образом:

    <receiver android:name=".KioskAppReceiver">
        <intent-filter>
            <action android:name="ru.headwind.kiosk.MESSAGE"/>
        </intent-filter>
    </receiver>

Далее Ваше приложение будет получать сообщения из киоска посредством переопределенных классом методов ресивера.

### Получение сообщений о ошибках обновления
В классе-наследнике ru.headwind.kiosk.sdk.KioskReceiver переопределите метод

    public void onUpdateErrorReceived(@NonNull Context context, @NonNull UpdateError updateError)

В случае возникновения ошибки обновления этот метод будет вызван, 
в объекте UpdateError будет приведена вся необходмая информация о ошибке.

Подробнее о типах и ключах можно прочитать в JavaDoc проекта

### Смена языка экрана обновления
Для этого вызовите следующий метод:

    KioskSdk.changeLocale(Context, Locale);
    
Обратите внимание, что параметр Locale помечен как nullable.
Если параметр Locale передать как null- Locale будет взят из параметра Context.
Обратите внимание, что киоск сохраняет последнее выставленное значение языка даже после перезагрузки устройства.
На данный момент киоск поддерживает следующие языковые коды:
    ru
    en
    

### JavaDoc
JavaDoc проекта находится в корне проекта в папке [sdk_javadoc](https://gitlab.com/headwind/kiosk-client-sdk/tree/4e58417ba08605cd890f314535e499c953c5847f/sdk_javadoc), 
так-же можно использовать проект как рабочий пример использования.

### Proguard
Настоятельно рекомендуется добавить в proguard-rules.pro:

    -keep class ru.headwind.kiosk.sdk.UpdateError {*;}