package ru.headwind.kioskcliensdkexample;

import android.content.Context;
import android.support.annotation.NonNull;

import ru.headwind.kiosk.sdk.KioskReceiver;
import ru.headwind.kiosk.sdk.UpdateError;

/**
 * Created by Ivan Lozenko on 09.03.2017.
 */

public class KioskAppReceiver extends KioskReceiver {

    @Override
    public void onUpdateErrorReceived(@NonNull Context context, @NonNull UpdateError updateError) {
        StringBuilder builder = new StringBuilder();
        builder.append("Type: ");
        builder.append(updateError.type);
        builder.append("\n");
        builder.append("Keys: \n");
        for (String key: updateError.info.keySet()){
            builder.append(key);
            builder.append(" : ");
            builder.append(updateError.info.getString(key));
            builder.append("\n");
        }
        context.getSharedPreferences(KioskAppReceiver.class.getSimpleName(), Context.MODE_PRIVATE)
                .edit()
                .putString(MainActivity.KEY_TEXT, builder.toString())
                .apply();
    }
}
