package ru.headwind.kioskcliensdkexample;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import ru.headwind.kiosk.sdk.KioskSdk;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_TEXT = "text";

    private TextView textView;

    private String currLang = "ru";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.textView);
        ((Button)findViewById(R.id.lang_button)).setText("Current Language " + currLang);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(KioskAppReceiver.class.getSimpleName(), MODE_PRIVATE);
        textView.setText(sharedPreferences.getString(KEY_TEXT, ""));
    }

    public void launchUpdate(View view){
        KioskSdk.launchScanStorages(this);
    }

    public void changeLanguage(View view){
        if (currLang.equals("ru")){
            currLang = "en";
        }else {
            currLang = "ru";
        }
        ((Button)view).setText("Current Language " + currLang);
        KioskSdk.changeLocale(this, new Locale(currLang));
    }
}
